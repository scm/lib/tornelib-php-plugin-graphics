<?php

use TorneLIB\TorneLIB_Plugin_JPGraph;
use PHPUnit\Framework\TestCase;

require_once('../vendor/autoload.php');

class TorneLIB_JPGraphTest extends TestCase {

	/** @var TorneLIB_Plugin_JPGraph */
	private $PLUG;

	function setUp() {
		$this->PLUG = new TorneLIB_Plugin_JPGraph();
	}

	function testPlug() {
		$this->assertTrue(is_object($this->PLUG));
	}

	function testCaptchaString() {
		$this->assertTrue(strlen($this->PLUG->getCaptchaString()) > 0 );
	}
	function testGetGraph() {
		$array = array(
			"values" => array(
				'a'=>1,
				'b'=>2
			)
		);
		$this->PLUG->setStructure($array);
		$this->PLUG->setWritePath("/tmp");
		echo $this->PLUG->getGraph();
	}
}
